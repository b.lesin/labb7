<?php

function print_ability($abil){
    if($abil=='imm')print 'Бессмертие';
    elseif($abil=='ph')print 'Прохождение сквозь стены';
    elseif($abil =='lv')print 'Левитация';
    else print 'Error';
    print '<br/>';
}
function print_data($data){
    print '<tr><td>';
    print $data['id'];
    print '</td><td>';
    print strip_tags($data['name']);
    print '</td><td>';
    print strip_tags($data['email']);
    print '</td><td>';
    print strip_tags($data['year']);
    print '</td><td>';
    print strip_tags($data['sex']);
    print '</td><td>';
    print intval($data['lb']);
    print '</td><td>';
    print strip_tags($data['bio']);
    print '</td><td>';
    print strip_tags($data['login']);
    print '</td><td>';
    print strip_tags($data['pass']);
    print '</td>';
}

function check_admin($db){
    if (empty($_SERVER['PHP_AUTH_USER']) ||
        empty($_SERVER['PHP_AUTH_PW'])) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="For lab6"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }

    $request = "SELECT * from admin";
    $result = $db->prepare($request);
    $result->execute();
    $flag=0;
    while($data=$result->fetch()){
        if($data['login']==strip_tags($_SERVER['PHP_AUTH_USER']) && password_verify($_SERVER['PHP_AUTH_PW'],$data['pass'])){
            $flag=1;
        }
    }
    if($flag==0){
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="For lab6"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
}