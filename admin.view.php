<html>
<head>
    <meta charset="utf-8"/>
    <title>Админка для 7 лабы</title>
    <link rel="stylesheet" media="all" href="style.css"/>

</head>
<body>
    <table class="table">
        <tr><th>ID</th><th>Имя</th><th>E-Mail</th><th>Дата рождения</th><th>Пол</th><th>Кол-во конечностей</th>
            <th>Биография</th><th>Логин</th><th>Хэш пароля</th><th>Способность</th><th>Действия</th></tr>
        <?php
            while($data = $result_info->fetch()) {
              print_data($data);
              print '<td>';
              if ($data_abil['id'] == $data['id']) {
                  do {
                      print_ability($data_abil['ability']);
                      $data_abil = $result_abil->fetch();
                  } while ($data_abil['id'] == $data['id'] && $data_abil);

              }
              print '</td>';
              $id = $data['id'];
              print '<td>';
              print "<a href='delete.php?id=$id'>Удалить</a>";
              print '<br/>';
              print "<a href='change.php?id=$id'>Изменить</a>";
              print '</td>';
              print'</tr>';
            }
        ?>
    </table>
    <h2>Статистика по сверхспособностям:</h2>
    <table class="table">
        <tr><th>Бессмертие</th><th>Прохождение сквозь стены</th><th>Левитация</th></tr>
        <tr>
            <td><?php print $data_im;?></td>
            <td><?php print $data_ph;?></td>
            <td><?php print $data_lv;?></td>
        </tr>
    </table>
</body>
