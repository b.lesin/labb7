<?php
/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
include 'db_info.php';
include 'functions.php';

check_admin($db);
print('Вы успешно авторизовались и видите защищенные паролем данные.');
print '<div>Привет админ!</div>';
print '<br/>';
if(isset($_COOKIE['del_rec'])){
    setcookie('del_rec','');
    print '<div>Запись удаленна</div>';
}
if(isset($_COOKIE['chg_rec'])){
    setcookie('chg_rec','');
    print '<div>Запись изменнена</div>';
}
// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********

    $request = "SELECT * from form7  order by id";
    $result_info = $db ->prepare($request);
    $result_info->execute();
    $request = "SELECT * FROM abil7 order by id";
    $result_abil = $db->prepare($request);
    $result_abil->execute();
    $data_abil = $result_abil->fetch();



    $request = "SELECT COUNT(ability) FROM abil7 where ability='imm' group by ability";
    $result = $db ->prepare($request);
    $result->execute();
    $data_im = $result->fetch()[0];
    $request = "SELECT COUNT(ability) FROM abil7 where ability='ph'";
    $result = $db ->prepare($request);
    $result->execute();
    $data_ph = $result->fetch()[0];
    $request = "SELECT COUNT(ability) FROM abil7 where ability='lv' group by ability";
    $result = $db ->prepare($request);
    $result->execute();
    $data_lv = $result->fetch()[0];

    include 'admin.view.php';
?>


</body>