<?php
header('Content-Type: text/html; charset=UTF-8');
include 'db_info.php';
include 'functions.php';

check_admin($db);

    $id = $db->quote($_GET['id']);
    $request = "SELECT * FROM form7 where id=$id";
    $sth = $db->prepare($request);
    $sth->execute();
    $data = $sth->fetch(PDO::FETCH_ASSOC);
    if ($data == false) {
        header('Location:admin.php');
        exit();
    }
    session_start();
    $_SESSION['login'] = $data['login'];
// Записываем ID пользователя.
    $_SESSION['uid'] =strip_tags($id);
    setcookie('chg_rec','1');
    setcookie('admin', '1');
// Делаем перенаправление.
   header('Location: index.php');
